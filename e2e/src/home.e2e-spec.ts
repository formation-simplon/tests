import { HomePage } from "./home.po";

describe("Homepage", () => {
  let page: HomePage;

  beforeEach(() => {
    page = new HomePage();
  });

  it("should display a link to enter the chat", () => {
    page.navigateTo();
    expect(page.getText()).toContain("Welcome to the app");
    expect(page.getLink()).toContain("/home");
  });
});
