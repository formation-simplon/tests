import { browser, by, element, $, $$ } from 'protractor';

export class HomePage {
  navigateTo() {
    return browser.get('/');
  }

  getText() {
    return $('a[routerLink]').getText();
  }

  getLink() {
    return element(by.css('h1')).getAttribute('routerLink');
  }
}
