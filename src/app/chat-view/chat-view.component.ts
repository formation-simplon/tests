import { Component, OnInit } from '@angular/core';
import { ChatService } from '../chat.service';
import { Message } from '../message';

@Component({
  selector: 'app-chat-view',
  templateUrl: './chat-view.component.html',
  styleUrls: ['./chat-view.component.css']
})
export class ChatViewComponent implements OnInit {

  messages: Message[]
  constructor(private chatService:ChatService) { }

  ngOnInit() {

    // souscription au service
    this.chatService.messages.subscribe(
      (messages) => this.messages = messages
    )
  }

  onDelete(id:string) {
    this.chatService.deleteMessage(id)
  }

}
