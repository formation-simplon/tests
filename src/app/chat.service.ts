import { Injectable } from '@angular/core';
import { Message } from './message';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ChatService {

  messages = new BehaviorSubject<Message[]>([
    {
      id: 'er',
      text: 'Bonjour tout le monde !',
      pseudo: 'Pauline'
    },
    {
      id: 'esdsdr',
      text: `Salut Pauline, on t'aime d'amour <3`,
      pseudo: 'Les apprenants'
    }
  ])

  constructor() { }

  // ajoute un messagethis.messages.value
  addMessage(pseudo, text) {
    let id = new Date().getTime().toString() + Math.round(Math.random()*99999999)
    let m:Message = { id, pseudo, text }
    let messages = this.messages.value
    messages.push(m)
    this.messages.next(messages)
    return m
  }

  // ajoute un message (asynchrone)
  addMessageAsync(pseudo, text) {
    const randomTime = Math.random()*2000;
    let promise = new Promise((resolve, reject) => {
      setTimeout(() => {
        this.addMessage(pseudo, text)
        resolve(true)
      }, randomTime)
    })
    return promise
  }

  // supprime un message 
  deleteMessage(id:string) {
    let index = this.messages.value.findIndex((m) => m.id == id)
    if(index >= 0) {
      let messages = Array.from(this.messages.getValue())
      messages.splice(index, 1)
      this.messages.next(messages)
      return true
    }
    return false
  }

  // met à jour un message
  updateMessage(id:string, text:string) {
    let m = this.getMessageById(id)
    m.text = text
    return m
  }

  // supprime tous les messages
  deleteAll() {
    this.messages.next([])
    return true
  }

  getMessageById(id:string) {
    return this.messages.value.find((m) => m.id == id)
  }

  // retourne le nombre de messages
  getCount() {
    return this.messages.value.length;
  }

  // renvoie un message aléatoire
  randomMessage() {
    if (!this.messages.value.length) return undefined
    let m = this.messages.value[Math.floor(Math.random()*this.messages.value.length)];
    return m
  }
}
