export interface Message {
    id: string,
    text: string,
    pseudo: string
}
