import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ChatService } from '../chat.service';

@Component({
  selector: 'app-chat-form',
  templateUrl: './chat-form.component.html',
  styleUrls: ['./chat-form.component.css']
})
export class ChatFormComponent implements OnInit {

  form: FormGroup

  constructor(private fb:FormBuilder, private chatService:ChatService) {
        // initialisation du formulaire
        this.form = this.fb.group({
          pseudo: '',
          text: '',
        })
   }

  ngOnInit() {



  }

  onSubmit() {
    console.log(this.form.value)
    const pseudo = this.form.value.pseudo
    const text = this.form.value.text
    this.chatService.addMessage(pseudo, text)
  }

}
