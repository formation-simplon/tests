import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule, FormsModule } from '@angular/forms'; 
import { RouterModule, Routes } from '@angular/router'

import { AppComponent } from './app.component';
import { ChatViewComponent } from './chat-view/chat-view.component';
import { ChatFormComponent } from './chat-form/chat-form.component';
import { HomeComponent } from './home/home.component';
import { ChatPageComponent } from './chat-page/chat-page.component';

const routes: Routes = [
  { path: '', component: HomeComponent},
  { path: 'chat', component: ChatPageComponent},
];


@NgModule({
  declarations: [
    AppComponent,
    ChatViewComponent,
    ChatFormComponent,
    HomeComponent,
    ChatPageComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes),
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
