import { TestBed, inject } from '@angular/core/testing';

import { ChatService } from './chat.service';

  // VOUS N'AVEZ PAS BESOIN DE MODIFIER QUE CE FICHIER POUR L'EXERCICE
  
  // Définissez votre suite de tests (describe(....))
  // Puis dans votre suite de tests ...
  
  // ...Instanciez UNE fois le ChatService
  
  /*
  beforeAll(() =>  {
    ...
  })
  */

   // ...Initialisez le service avec 4 messages avant chacun des tests

   // ...Et supprimez tous les messages après chaque test

   // ...Test: service.deleteAll() doit retourner true 

   // ...Test: deleteAll() doit retourner true ET le nouveau compte de messages égal à 0

   // ...Test: randomMessage() ne doit jamais retourner null

   // ...Test: deux messages devraient avoir des ID différents

   // ...Test: randomMessage() renvoie un objet ayant les propriétés id, text et pseudo définies

   // ...Test: ajouter un message (addMessage) devrait incrémenter le compte total de 1

   // ...Test: Ajouter un message via addMessageAsync() fait augementer le compte de 1 (ne PAS utiliser tick ou fakeAsync ni setTimeout) . Attention, asynchrone !
  
   // ...Test: supprimer un message devrait décrémenter le compte total de 1

   // ...Test: mettre a jour le texte d'un message fonctionne

